from glob import glob
import PIL.Image
from PIL import ImageDraw, ImageFont
import math
import optparse
import os
import sys
from numpy import zeros
import deepzoom as dz
from functools import reduce


def commonExt(all_files):
    """ the most common file extension in a list """
    all_ext = map(lambda x: x.split(".")[-1], all_files)
    ext_dict = dict()
    for ext in all_ext:
        ext_dict[ext] = ext_dict.get(ext, 0) + 1
    return filter(
        lambda x: x[1] == max(
            ext_dict.values()),
        ext_dict.items())[0][0]


def drawText(cur_img, text, pos=(0, 0), mode='RGB', txt_len=100):
    """ Draw the text in a way that it is txt_len pixels long
    """
    #TEXTCOLOR = (255, 0, 0)
    cur_len = txt_len + 1
    fontsize = 0
    # determine the correct font size
    while cur_len < txt_len:
        fontsize += 1
        #font = ImageFont.truetype('Arial.ttf', fontsize)
        #(cur_len,cur_height)=ImageDraw.Draw(cur_img).textsize(text,font=font)
    ImageDraw.Draw(cur_img).text(pos, text)
logConvTable = map(
    lambda x: int(
        math.log(
            x +
            1) *
        255.0 /
        11.09),
    range(65536))

debug_msg = False


class MontageFromStack(object):

    """ Creates a virtual montage (tiled image) from a folder
    For the purposes of this tool the tiles in the montage will be called panes
    to avoid confusion with tiles in the deepzoom
    """
    _imgCache = dict()

    def __init__(
            self,
            path,
            im_scale='log',
            im_step=1,
            useCache=True,
            postLoadOperations=lambda x: x):
        # prereading setup
        self.path = path
        all_files = glob(path + "/*.*")
        keepExt = commonExt(all_files)
        files = sorted(
            filter(lambda x: x.split(".")[-1] == keepExt, all_files))
        self.files = files[1:len(files):im_step]
        self.im_scale = im_scale
        if len(self.files) > 100:
            self.useCache = useCache
        else:
            self.useCache = False
        self.useCache = useCache
        self.add_filename = False  # do not add the filename
        self.write_pane_id = False  # write the slice names into the panes
        # rotation, rescaling, cropping etc
        self.postLoadOperations = postLoadOperations
        # now a file can be read
        fImage = self._open(0)
        self.base_size = fImage.size
        self.mode = fImage.mode
        self.img_count = len(self.files)
        self.mt_rows = int(math.ceil(math.sqrt(self.img_count)))
        self.mt_cols = int(math.ceil(self.img_count / float(self.mt_rows)))
        print ('Montage Size:', self.mt_cols, 'by', self.mt_rows)
        self.size = (
            self.base_size[0] *
            self.mt_rows,
            self.base_size[1] *
            self.mt_cols)

    def wrappedOpenFunction(self, path, im_scale):
        img = PIL.Image.open(path)
        if img.mode == "I;16":
            # two options either linear
            if im_scale == 'log':
                return img.convert("I").point(logConvTable, 'L').convert('RGB')
            else:
                return img.convert("I").point(
                    lambda x: x * (255.0 / 65535.0) + 0, 'RGB')
        else:
            return img

    def _open(self, i):
        assert i >= 0 and i < len(self.files), "invalid position:" + str(i)
        if i in self._imgCache:
            return self._imgCache[i]
        else:
            cur_img = self.wrappedOpenFunction(
                self.files[i],
                im_scale=self.im_scale)  # this can be modified to add a little name to the file
            cur_img = self.postLoadOperations(cur_img)
            if self.add_filename:
                drawText(cur_img,
                         self.files[i].split('/')[-1],
                         (0,
                          0),
                         txt_len=cur_img.size[1] / 2)  # add filename
            if self.useCache:
                self._imgCache[i] = cur_img
            return cur_img

    def crop(self, bounds):
        return self._assembleImage(
            self.size,
            bounds,
            imfilter=PIL.Image.NEAREST)

    def resize(self, new_size, imfilter=PIL.Image.ANTIALIAS):
        return self._assembleImage(new_size, imfilter=imfilter)

    def getPanesInBounds(self, bounds):
        paneList = []
        new_size = self.size
        pane_size = (int(math.floor(new_size[
                     0] / float(self.mt_cols))), int(math.floor(new_size[1] / float(self.mt_rows))))
        if debug_msg:
            print ('Pane Size', pane_size, 'Tile Crop', bounds)
        for i in range(self.mt_rows):
            for j in range(self.mt_cols):
                read_pane = True
                c_pane_ind = i * self.mt_rows + j
                if c_pane_ind >= self.img_count:
                    read_pane = False  # no image here
                start_pos = (i * pane_size[0], j * pane_size[1])
                end_pos = (
                    start_pos[0] +
                    pane_size[0],
                    start_pos[1] +
                    pane_size[1])

                # dont read the image unless it is needed
                if start_pos[0] >= bounds[2]:
                    read_pane = False
                if start_pos[1] >= bounds[3]:
                    read_pane = False
                if end_pos[0] <= bounds[0]:
                    read_pane = False
                if end_pos[1] <= bounds[1]:
                    read_pane = False
                if read_pane:
                    if debug_msg:
                        print (
                            'Reading Pane',
                            c_pane_ind,
                            (i,
                             j),
                            start_pos,
                            end_pos)
                    # read, resize, and crop in one fell swoop (it is faster through PIL than Python)
                    # the position to start writing the current output pane (at least the start position
                    # but possible further)
                    s_write_pos = (
                        max(bounds[0], start_pos[0]), max(bounds[1], start_pos[1]))
                    # the boundaries to crop the pane with
                    c_pane_bounds = (s_write_pos[0] -
                                     start_pos[0], s_write_pos[1] -
                                     start_pos[1], min(bounds[2] -
                                                       s_write_pos[0], pane_size[0]), min(bounds[3] -
                                                                                          s_write_pos[1], pane_size[1]))
                    paneList += [{"x": i,
                                  "y": j,
                                  "i": c_pane_ind,
                                  "bnd": c_pane_bounds}]
        return paneList

    def _assembleImage(self, new_size, bounds=0, imfilter=PIL.Image.ANTIALIAS):
        """ Assemble the image at the proper scale defined by new_size
        with the right region of interest defined by crop
        """
        if bounds == 0:
            bounds = (0, 0, new_size[0], new_size[1])
        # bounds is min max, cropbnd is min + dimensions
        bnd_size = (bounds[2] - bounds[0], bounds[3] - bounds[1])
        rs_image = PIL.Image.new(self.mode, bnd_size)
        pane_size = (int(math.floor(new_size[
                     0] / float(self.mt_rows))), int(math.floor(new_size[1] / float(self.mt_cols))))
        if debug_msg:
            print ('Pane Size', pane_size, 'Tile Crop', bounds)
        if pane_size[0] < 1 or pane_size[1] < 1:
            return rs_image  # deepzoom requests some goofy shit
        paneIds = []
        for i in range(self.mt_rows):
            for j in range(self.mt_cols):
                read_pane = True
                c_pane_ind = i * self.mt_rows + j
                if c_pane_ind >= self.img_count:
                    read_pane = False  # no image here
                start_pos = (i * pane_size[0], j * pane_size[1])
                end_pos = (
                    start_pos[0] +
                    pane_size[0],
                    start_pos[1] +
                    pane_size[1])

                # dont read the image unless it is needed
                if start_pos[0] >= bounds[2]:
                    read_pane = False
                if start_pos[1] >= bounds[3]:
                    read_pane = False
                if end_pos[0] <= bounds[0]:
                    read_pane = False
                if end_pos[1] <= bounds[1]:
                    read_pane = False

                if (read_pane):
                    paneIds += [c_pane_ind]
                    if debug_msg:
                        print (
                            'Reading Pane',
                            c_pane_ind,
                            (i,
                             j),
                            start_pos,
                            end_pos)
                    # read, resize, and crop in one fell swoop (it is faster through PIL than Python)
                    # the position to start writing the current output pane (at least the start position
                    # but possible further)
                    s_write_pos = (
                        max(bounds[0], start_pos[0]), max(bounds[1], start_pos[1]))
                    # the boundaries to crop the pane with
                    c_pane_bounds = (
                        s_write_pos[0] - start_pos[0], s_write_pos[1] - start_pos[1], min(
                            bounds[2], pane_size[0]), min(
                            bounds[3], pane_size[1]))

                    c_pane = self._open(c_pane_ind).resize(
                        pane_size,
                        imfilter).crop(c_pane_bounds)
                    c_pane_size = c_pane.size

                    #print ('Top-corner',s_write_pos,'->')
                    writeCounts = 0
                    # this is real slow, but it lets me ignore types and byte
                    # arrays
                    for x in range(c_pane_size[0]):
                        for y in range(c_pane_size[1]):
                            write_pos = (
                                s_write_pos[0] +
                                x -
                                bounds[0],
                                s_write_pos[1] +
                                y -
                                bounds[1])  # position in output image
                            if (
                                    write_pos[0] >= 0) and (
                                    write_pos[1] >= 0) and (
                                    write_pos[0] < bnd_size[0]) and (
                                    write_pos[1] < bnd_size[1]):
                                #print ('Writing',(x,y),'->',write_pos)
                                rs_image.putpixel(
                                    write_pos, c_pane.getpixel(
                                        (x, y)))
                                writeCounts += 1
                    if debug_msg:
                        print {'aSub-region': c_pane_bounds, 'cpane': c_pane_size, 'wc': writeCounts}
        if self.write_pane_id:
            # code to collapse the names for having pane ids
            paneNames = map(
                lambda id: '.'.join(self.files[id].split('/')[-1].split('.')[0:-1]), paneIds)
            if len(paneNames) > 1:
                check_len = lambda bl, bs, txt_len: all(
                    map(lambda x: x.startswith(bs[0:txt_len]), bl))
                i = 0
                while check_len(paneNames, paneNames[0], i + 1):
                    i += 1
                panePrefix = paneNames[0][0:i] + ':'
                paneSNames = map(lambda cName: cName[i:], paneNames)
            else:
                panePrefix = ''
                paneSNames = paneNames

            drawText(
                rs_image,
                panePrefix +
                ','.join(paneSNames),
                (0,
                 0),
                txt_len=rs_image.size[1] /
                2)  # add filename
        return rs_image


def stackParser(parser):
    """ The stack and dz arguments """
    parser = dz.dzParser(parser)
    parser.add_option(
        '-k',
        '--im_skip',
        dest='im_skip',
        type='int',
        default=1,
        help='Step between images (1 is all images, 2 is every other, etc). Default: 1')
    parser.add_option(
        '-c',
        '--color_scale',
        dest='color_scale',
        default='log',
        help='Use linear or logarithmic scale for 16-bit images, Default: log')

    parser.add_option('-t', '--test', dest='test', action="store_true",
                      default=False, help='Run as a test. Default: False')
    parser.add_option(
        '-X',
        '--execute_command',
        dest='execute_command',
        default='',
        help='Execute command on image comma between arguments, ZZ between commands, example: -x crop,0,0,100,100ZZresize,50,50')
    return parser


def createImageCreator(source, options):

    if not options.destination:
        if os.path.exists(source):
            options.destination = os.path.splitext(source)[0] + '.dzi'
        else:
            options.destination = os.path.splitext(
                os.path.basename(source))[0] + '.dzi'
    if options.resize_filter and options.resize_filter in dz.RESIZE_FILTERS:
        options.resize_filter = dz.RESIZE_FILTERS[options.resize_filter]
    ploFun = lambda x: x
    if len(options.execute_command) > 0:
        excmd = map(
            lambda ccmd: ccmd.split(','),
            options.execute_command.split('ZZ'))
        funList = []
        for cCmd in excmd:
            print (cCmd, cCmd[0])
            if cCmd[0] == 'crop':
                funList += [
                    lambda x: x.crop(
                        (int(
                            cCmd[1]), int(
                            cCmd[2]), int(
                            cCmd[3]), int(
                            cCmd[4])))]
            if cCmd[0] == 'resize':
                funList += [lambda x: x.resize((int(cCmd[1]), int(cCmd[2])))]
        ploFun = reduce(lambda a, b: lambda x: a(b(x)), funList)
    mfsFun = lambda path: MontageFromStack(
        path,
        im_scale=options.color_scale,
        im_step=options.im_skip,
        postLoadOperations=ploFun)
    if options.test:
        mfs = mfsFun(source)
        return mfs.resize(
            (int(mfs.size[0] / 2), int(mfs.size[1] / 2))).save(options.destination + '.png')

    dz.loadImageFunction = mfsFun
    return dz.ImageCreator(tile_size=options.tile_size,
                           tile_format=options.tile_format,
                           image_quality=options.image_quality,
                           resize_filter=options.resize_filter)


def main():
    parser = optparse.OptionParser(usage='Usage: %prog [options] filename')

    parser = stackParser(parser)

    (options, args) = parser.parse_args()

    if not args:
        parser.print_help()
        sys.exit(1)

    source = args[0]
    creator = createImageCreator(source, options)
    creator.create(source, options.destination, options.voxel_size)


if __name__ == '__main__':
    main()
