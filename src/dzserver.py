
import os
import sys
import deepzoom as dz
from dzfromstack import MontageFromStack, stackParser, createImageCreator

import SocketServer
import SimpleHTTPServer
import urllib
try:
    import cStringIO
    StringIO = cStringIO
except ImportError:
    import StringIO


HTTP_PATH = '/'.join(dz.SCRIPT_PATH.split('/')[0:-2])
dzImageCache = dict()
tileCache = dict()


def createTileFun(source, options):

    creator = createImageCreator(source, options)
    (tileFun, desc) = creator.createTileService(source, options.voxel_size)
    return (tileFun, desc)


class DZHTTPHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def makeResponse(self, msg="Hello"):
        # Construct a server response.
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(msg)

    def sendImage(self, image, format='jpeg'):
        self.send_response(200)
        self.send_header('Content-type', 'image/' + format)
        #self.send_header('Content-Disposition', 'attachment; filename=test.xlsx')
        #self.send_header('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        self.end_headers()
        buf = StringIO.StringIO()
        image.save(buf, format=format)
        jpeg = buf.getvalue()
        self.wfile.write(jpeg)

    def writeDZISite(self, imgName, desc):
        # Code to write the HTML file
        templines = desc.makeHTMLLines(imgName + '/')
        return ''.join(templines)

    def staticRoute(self):
        """ fetch all static requests from the html directory """
        newpath = HTTP_PATH + '/html' + self.path
        print 'Rerouted: ' + self.path + ' to ' + newpath
        ctype = self.guess_type(newpath)
        try:
            # Always read in binary mode.
            f = open(newpath, 'rb')
        except IOError:
            self.send_error(404, "File " + newpath + " not found")
            return None
        self.send_response(200)
        self.send_header("Content-type", ctype)
        fs = os.fstat(f.fileno())
        self.end_headers()
        self.copyfile(f, self.wfile)
        return

    def do_GET(self):
        global options
        cpath = self.path.strip().split('/')
        print 'Request: ' + self.path + ' as ' + str(cpath)
        if self.path.strip() == '/':
            return SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)
        if self.path.strip() == '/show_cache':
            return self.makeResponse("<p> Images: " +
                                     str(dzImageCache.keys()) +
                                     "<p> Positions: " +
                                     str(tileCache.keys()))
        if len(cpath) >= 2:  # requesting image
            imgName = cpath[1]
            if os.path.isdir(imgName):
                (tileFun, desc) = dzImageCache.get(
                    imgName, createTileFun(imgName, options))
                dzImageCache[imgName] = (tileFun, desc)
                if len(cpath) < 4:
                    self.makeResponse(self.writeDZISite(imgName, desc))
                    return
                elif len(cpath) == 4:
                    level = cpath[2]
                    rcVal = cpath[3]
                    (col, row) = rcVal[0:rcVal.rfind(".")].split('_')
                    print 'Requestiong:' + str({"level": level, "col": col, "row": row})
                    tilePos = (int(level), int(col), int(row))
                    tileImg = tileCache.get(
                        tilePos,
                        tileFun(
                            tilePos[0],
                            tilePos[1],
                            tilePos[2]))
                    tileCache[tilePos] = tileImg
                    self.sendImage(tileImg)
                    return
        # for all other scenerios use static routing
        return self.staticRoute()


def getOptions():
    import optparse
    parser = optparse.OptionParser(usage='Usage: %prog [options] filename')
    parser = stackParser(parser)
    parser.add_option(
        '-p',
        '--port',
        dest='port',
        type='int',
        default=8080,
        help='Port to use: 8080')
    (options, args) = parser.parse_args()
    return options


if __name__ == '__main__':
    options = getOptions()
    Handler = DZHTTPHandler
    server = SocketServer.ForkingTCPServer(('0.0.0.0', options.port), Handler)
    server.serve_forever()
