setBatchMode("hide");
colorSuffix="_rgb";

imagesToLoad = getArgument;
if (imagesToLoad=="") imagesToLoad=getDirectory("Select the folder with the raw images");
if (File.isDirectory(imagesToLoad)) {
    run("Image Sequence...", "open="+imagesToLoad+" sort");
} else {
    open(imagesToLoad);
}

run("Enhance Contrast", "saturated=0.35");
run("Thermal");
run("RGB Color");
outpath = File.getParent(imagesToLoad)+File.separator+File.getName(imagesToLoad)+colorSuffix;
File.makeDirectory(outpath);
run("Image Sequence... ", "format=JPEG start=0 digits=4 save="+outpath);
run("Close");

if (getArgument=="") setBatchMode("exit and display");
else exit("Finished creating:"+outpath);