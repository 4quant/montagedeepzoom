# Overview
MontageDeepZoom creates a DeepZoom image (googlemaps-like) from a folder of TIF images by first converting it into a montage and making the deepzoom from this. For memory reasons reasons, it never loads all of the images at the same time only the ones needed for the given tile.

# Fastest Performance
Using larger tiles (512) increases the computational rate substantially, also -k allows images to be skipped so the following makes only every 100 images


# Example Commands
## Jereon Data

- Tomcat 
```
nohup time python src/dzfromstack.py /gpfs/home/mader/JeroenData/tomcat/Sample_20_A/rec_8bit_parzen/ -d html/tomcat.dzi -X crop,420,420,2100,2100 -k 50 -s 512 -v 0.33e-6 &
```

```
nohup time python src/dzfromstack.py /gpfs/home/mader/JeroenData/tomcat/Sample_20_A/dto_slices/ -d html/tomcat_dto.dzi -k 50 -s 512 -v 0.33e-6 &
```


- CSAXS
```
nohup python src/dzfromstack.py /gpfs/home/mader/JeroenData/csaxs/Sample_20_A/ufilt -d html/csaxs.dzi -k 10 -s 512 -v 14.3e-9 &
```

-CSAXS DTO Data
```
nohup python src/dzfromstack.py /gpfs/home/mader/JeroenData/csaxs/Sample_20_A/dto_stack -d html/csaxs_dto.dzi -k 10 -s 512 -v 14.3e-9 &
```


## ImageJ Macro
```
/afs/psi.ch/project/tipl/jar/Fiji.app/ImageJ-linux64 --mem=8G -batch -macro src/makeColorStack.ijm /gpfs/home/mader/JeroenData/tomcat/Sample_20_A/neighbors.tif
```

-with a directory
```
/afs/psi.ch/project/tipl/jar/Fiji.app/ImageJ-linux64 --mem=8G -macro src/makeColorStack.ijm /gpfs/home/mader/JeroenData/csaxs/Sample_20_A/ufilt/
```
nohup python src/dzfromstack.py /gpfs/home/mader/JeroenData/csaxs/Sample_20_A/ufilt_rgb -d html/csaxs_rgb.dzi -k 10 -s 512 -v 14.3e-9 &
```
